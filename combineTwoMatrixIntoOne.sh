# combine two gene expression matrix into one
set -eu

file1Matrix=$1
file1Group=$2
file2Matrix=$3
file2Group=$4
combinedPrefix=$5

separator="space" # either space or tab
if [ "$#" -ge 6 ]; then
  separator=$6
fi

# check to make sure all genes are in the same order
awk '(NR > 1) {print $1}' ${file1Matrix} > ${combinedPrefix}.temp1
awk '(NR > 1) {print $1}' ${file2Matrix} > ${combinedPrefix}.temp2
diff ${combinedPrefix}.temp1 ${combinedPrefix}.temp2
# continue if no difference
if [ $separator == "space" ]
then
  echo "space separated"
  paste -d' ' ${file1Matrix} <(cut -d' ' -f1 --complement ${file2Matrix}) > ${combinedPrefix}.txt
elif [ $separator == "tab" ]
then
  echo "tab separated"
  paste ${file1Matrix} <(cut -f1 --complement ${file2Matrix}) > ${combinedPrefix}.txt
else
  echo "wrong separator specified"
  rm ${combinedPrefix}.temp1 ${combinedPrefix}.temp2
  exit 1
fi

cat ${file1Group} ${file2Group} > ${combinedPrefix}.group
rm ${combinedPrefix}.temp1 ${combinedPrefix}.temp2
