# Single cell RNA-seq data processing, and DE analysis using different packages

## QC and preprocessing
The script file specificDataProcess/singleCellDatasetQC.sh performs preprocessing of different data sets assuming they are downloaded and unzipped

## DE code
The R file DEFromOtherPackages.R shows how other methods of differential expression analysis from other packages are used.

