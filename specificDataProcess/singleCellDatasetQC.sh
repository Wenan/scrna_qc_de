## QC code for different data sets
## It assumes it has been downloaded and unzipped in certain subfolders 
## Check the comments for more explanation

# make sure ${codeFolder} matches the folder storing R code cleanUMIData.R
codeFolder=".."
# make sure this is the folder containing all subfolders of data sets
outputRoot="/research/projects/chen1grp/scRNA_Dev/common/testDataQC/dataRoot/"
# Ensemble ID and gene name table
mouseEnsID="./genes_mm10.tsv"

cd ${codeFolder}

#### Klein_2015_inDrop
# folder: ${outputRoot}/Klein_2015_inDrop/
# bzip2 -d ${outputRoot}/Klein_2015_inDrop/GSM1599494_ES_d0_main.csv.bz2
Rscript specificDataProcess/prepareKleinInDrop.R ${outputRoot}/Klein_2015_inDrop/GSM1599494_ES_d0_main.csv ${mouseEnsID}
# no further QC is applied
######################

#### Grun_2015_CEL-Seq ##############
# folder: ${outputRoot}/Grun_2015_CEL-Seq/
gzip -d ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos.txt.gz
gzip -d ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_Whole_Organoid_Replicate_2.txt.gz
# YFPPos
Rscript addEntrezIDAndGroupFile.R ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos.txt ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos_ens ${mouseEnsID}  ./GO_cellCycle_mm.txt
# while organoid
Rscript addEntrezIDAndGroupFile.R ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_Whole_Organoid_Replicate_2.txt ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_Whole_Organoid_Replicate_2_ens ${mouseEnsID} ./GO_cellCycle_mm.txt
# remove ERCC
for dataPrefix in ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos_ens ${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_Whole_Organoid_Replicate_2_ens
do
  sed -e '/NA_ERCC-/ d' ${dataPrefix}.txt > ${dataPrefix}.intrinsic.txt
  cp ${dataPrefix}.group ${dataPrefix}.intrinsic.group 
done

# QC
dataPrefix="${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos_ens.intrinsic"
Rscript cleanUMIData.R ${dataPrefix}.txt ${dataPrefix}.group ${dataPrefix}.QC 500 20000 0.1
dataPrefix="${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_Whole_Organoid_Replicate_2_ens.intrinsic"
Rscript cleanUMIData.R ${dataPrefix}.txt ${dataPrefix}.group ${dataPrefix}.QC 500 20000 0.1

# remove outliers for DE simulations
filePrefix="${outputRoot}/Grun_2015_CEL-Seq/GSE62270_data_counts_YFPpos_ens.intrinsic.QC"
# extract one plate
Rscript selectColumns.R ${filePrefix}.txt ${filePrefix}.I5d.txt "\\bI5d|gene"
# create the group file
filePrefixNew="${filePrefix}.I5d"
nCell=$(( $(awk '(NR == 1) {print NF}' ${filePrefixNew}.txt) - 1 ))
echo -n "" > ${filePrefixNew}.group
for ((i = 1; i <= ${nCell}; i++)) 
do
  echo "1" >> ${filePrefixNew}.group
done
Rscript removeOutliers.R ${filePrefix}.I5d.txt ${filePrefix}.I5d.group ${filePrefix}.I5d.T30 30
########################################

## Jaitin ###
# folder: ${outputRoot}/Jaitin_2014_MARS-Seq/
# remove 3 genes with different number of genes and ERCC genes
sed -e '5512d;5586d;19859d' ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab.txt | sed -e '/^ERCC/ d' > ${outputRoot}//Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.txt
# use all
Rscript addEntrezIDAndGroupFile.R ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.txt ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3_ens ./genes_mm10.tsv ./GO_cellCycle_mm.txt
# need QC because the total UMI can be 0
Rscript cleanUMIData.R ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3_ens.txt ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3_ens.group ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3_ens.QC 
# format file ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_experimental_design.txt
tr $'\r' $'\n' < ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_experimental_design.txt | sed '1,6s/^/#/' > ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_experimental_design.formatted.txt
# separate CD11c+ and CD11c+(2hr_LPS) cells
Rscript specificDataProcess/prepareJaitin.R "${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.txt" "./genes_mm10.tsv" "${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_experimental_design.formatted.txt"

# QC
Rscript cleanUMIData.R ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+.ens.txt ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+.ens.group ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+.ens.QC 500 4000 0.1
Rscript cleanUMIData.R ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+2hr_LPS.ens.txt ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+2hr_LPS.ens.group ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+2hr_LPS.ens.QC 500 1500 0.1

# remove outlier for DE simulation, no cell removed
for filePrefix in ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+.ens.QC ${outputRoot}/Jaitin_2014_MARS-Seq/GSE54006_umitab_remove3.CD11c+2hr_LPS.ens.QC
do
  Rscript removeOutliers.R ${filePrefix}.txt ${filePrefix}.group ${filePrefix}.noOutlierT30 30
done
#############

#### Islam 2011 ####
# folder: ${outputRoot}/Islam_2011_STRT-Seq
# format it to GSE29087_L139_expression_tab.formatted.txt
sed  '1,3d;5,6d' ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.txt | cut -f2-7 --complement | sed '1s/^/geneName/' > ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.formatted.txt
# remove spike in, genes with r_ and keep the 48 ESC
sed -e '/^RNA_SPIKE/d' ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.formatted.txt | sed -e '/^r_/d' | cut -f1-49 > ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.formatted.noSpike.txt
Rscript addEntrezIDAndGroupFile.R ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.formatted.noSpike.txt ${outputRoot}/Islam_2011_STRT-Seq/GSE29087_L139_expression_tab.formatted.noSpike.ens ./genes_mm10.tsv ./GO_cellCycle_mm.txt
#################

#### Ziegenhain_6platforms_2016 ####
## folder: ${outputRoot}/Ziegenhain_2016_6platforms
Rscript specificDataProcess/process6Platforms.R "${outputRoot}/Ziegenhain_2016_6platforms/GSE75790_ziegenhain_complete_data.txt" "${outputRoot}/Ziegenhain_2016_6platforms/GSE75790"
# read count based
## folder: ${outputRoot}/Ziegenhain_2016_6platforms_read
## This data is requested from the author of the paper 
# reformat the header to be the same as the UMI based file
perl -pe 's/\b\S*?_//g' ${outputRoot}/Ziegenhain_2016_6platforms_read/Ziegenhain_etal2017_readcounts.txt > ${outputRoot}/Ziegenhain_2016_6platforms_read/Ziegenhain_etal2017_readcounts_formatted.txt
Rscript specificDataProcess/process6Platforms.R "${outputRoot}/Ziegenhain_2016_6platforms_read/Ziegenhain_etal2017_readcounts_formatted.txt" "${outputRoot}/Ziegenhain_2016_6platforms_read/GSE75790"

## combine batch A and B for DE analysis
dataFolder=${outputRoot}/Ziegenhain_2016_6platforms
mkdir -p ${outputRoot}/Ziegenhain_2016_6platforms/DEAnalysis
for protocol in CELseq2 DropSeq MARSseq SCRBseq
do
  # combine A and B together
  datasetA="${dataFolder}/GSE75790_${protocol}A.intrinsic"
  datasetB="${dataFolder}/GSE75790_${protocol}B.intrinsic"
  bash combineTwoMatrixIntoOne.sh ${datasetA}.txt ${datasetA}.group ${datasetB}.txt ${datasetB}.group ${outputRoot}/Ziegenhain_2016_6platforms/DEAnalysis/${protocol} tab
  Rscript addGeneSymbol.R ${outputRoot}/Ziegenhain_2016_6platforms/DEAnalysis/${protocol}.txt ${outputRoot}/Ziegenhain_2016_6platforms/DEAnalysis/${protocol}.txt ./genes_mm10.tsv
done

## remove outliers for DE simulation
folderName="Ziegenhain_2016_6platforms" # UMI based
threshold=30
for dataName in CELseq2 SCRBseq
do
  dataPrefix=${outputRoot}/Ziegenhain_2016_6platforms/DEAnalysis/${dataName}
  Rscript removeOutliers.R ${dataPrefix}.txt ${dataPrefix}.group ${dataPrefix}.noOutlierT${threshold} ${threshold}
  #cp ${dataPrefix}.noOutlierT${threshold}.group ${dataPrefix}.noOutlierT${threshold}.2.group
  #sed -i 's/A\|B//' ${dataPrefix}.noOutlierT${threshold}.group
done
####################################

## Tn Tm Zheng et al. 2017
## folder: ${outputRoot}/PBMC10xAnalysis/Tn/filtered_matrices_mex/hg19 and ${outputRoot}/PBMC10xAnalysis/Tn/filtered_matrices_mex/hg19
# convert to a matrix txt and group txt
for dataName in "Tn" "Tm"
do
  Rscript specificDataProcess/PBMC10xFromMTX.R ${outputRoot}/PBMC10xAnalysis/${dataName}/filtered_matrices_mex/hg19/matrix.mtx ${outputRoot}/PBMC10xAnalysis/${dataName}/filtered_matrices_mex/hg19/genes.tsv ${outputRoot}/PBMC10xAnalysis/${dataName}/filtered_matrices_mex/hg19/barcodes.tsv ${outputRoot}/PBMC10xAnalysis/${dataName} ${dataName}
done

# QC data
dataName="Tn"
Rscript cleanUMIData.R ${outputRoot}/PBMC10xAnalysis/${dataName}.txt ${outputRoot}/PBMC10xAnalysis/${dataName}.group ${outputRoot}/PBMC10xAnalysis/QC/${dataName} 500 3000 0.1
dataName="Tm"
Rscript cleanUMIData.R ${outputRoot}/PBMC10xAnalysis/${dataName}.txt ${outputRoot}/PBMC10xAnalysis/${dataName}.group ${outputRoot}/PBMC10xAnalysis/QC/${dataName} 500 4000 0.1

# remove outerlier for DE simulation, no cells removed
filePrefix="${outputRoot}/PBMC10xAnalysis/QC/Tm"
Rscript removeOutliers.R ${filePrefix}.txt ${filePrefix}.group ${filePrefix}.noOutlierT30 30


