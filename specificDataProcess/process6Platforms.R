source("loadData.R")

main = function() {
  args = commandArgs(T)
  inputFile = args[1]
  outputPrefix = args[2] 
  data = read.table(inputFile, header = T, as.is = T)
  platformName = sub("_.*", "", colnames(data))
  indexERCC = grep("ERCC", rownames(data))
  platformUnique = unique(platformName)
  #browser()
  for (i in 1:length(platformUnique)) {
    indexRow = which(platformName == platformUnique[i])
    # write the ERCC
    writeData(data[indexERCC, indexRow, drop = F],
              rep(platformUnique[i], length(indexRow)),
              paste0(outputPrefix, "_", platformUnique[i], ".ERCC.txt"),
              paste0(outputPrefix, "_", platformUnique[i], ".ERCC.group"))
    
    # write true genes 
    writeData(data[-indexERCC, indexRow, drop = F],
              rep(platformUnique[i], length(indexRow)),
              paste0(outputPrefix, "_", platformUnique[i], ".intrinsic.txt"),
              paste0(outputPrefix, "_", platformUnique[i], ".intrinsic.group"))
    
  }
}

main()
